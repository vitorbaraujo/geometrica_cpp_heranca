#ifndef GEOMETRICA_H
#define GEOMETRICA_H

#include<iostream>

class geometrica{
	private:
		float base,altura;
	public:
		geometrica();
		geometrica(float base,float altura);
		
		float getBase();
		float getAltura();
		void setBase(float base);
		void setAltura(float altura);

/*	método abaixo não vou implementar na classe geometrica(abstrata), porém as classes que herdam tem que que implementar	*/		
		virtual float area() = 0;
};

#endif
