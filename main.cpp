#include "triangulo.hpp"
#include "quadrado.hpp"

#include<iostream>

using namespace std;

int main(){

	geometrica * objTriangulo = new triangulo(30,30);
	geometrica * objQuadrado = new quadrado(50,14); 
	cout << "Area do triangulo: " << objTriangulo->area() << endl;
	cout << "Area do quadrado: " << objQuadrado->area() << endl;

	return 0;
}

